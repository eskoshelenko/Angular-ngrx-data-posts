import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit {
  editPostForm!: FormGroup
  id?: string

  constructor(
    private postsService: PostsService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.editPostForm = new FormGroup({
      title: new FormControl(null, [
        Validators.required, 
        Validators.minLength(6)
      ]),
      description: new FormControl(null, [
        Validators.required, 
        Validators.minLength(10)
      ])
    })
    this.id = this.route.snapshot.params['id']
    this.postsService.entities$.subscribe((posts) => {
      if(posts.length) {
        const post = posts.find(post => post.id === this.id)

        this.editPostForm.patchValue({
          title: post?.title,
          description: post?.description
        })
      }
    })
  }

  onEditPostSubmit() {
    const postData = { ...this.editPostForm.value, id: this.id }

    this.postsService.update(postData)
    this.router.navigate(['/posts'])
  }
}
