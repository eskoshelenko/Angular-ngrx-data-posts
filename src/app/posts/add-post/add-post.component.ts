import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Post } from 'src/app/models/post.model';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent implements OnInit {
  addPostForm!: FormGroup

  constructor(
    private postsService: PostsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.addPostForm = new FormGroup({
      title: new FormControl(null, [
        Validators.required, 
        Validators.minLength(6)
      ]),
      description: new FormControl(null, [
        Validators.required, 
        Validators.minLength(10)
      ])
    })
  }

  onAddPostSubmit() {
    const post: Post = this.addPostForm.value

    this.postsService.add(post).subscribe(data => {
      this.router.navigate(['/posts'])
    })
  }
}
