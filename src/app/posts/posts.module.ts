import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { EntityDataService, EntityDefinitionService, EntityMetadataMap } from "@ngrx/data";
import { Post } from "../models/post.model";
import { AddPostComponent } from "./add-post/add-post.component";
import { EditPostComponent } from "./edit-post/edit-post.component";
import { PostsDataService } from "./posts-data.service";
import { PostsListComponent } from "./posts-list/posts-list.component";
import { PostsResolver } from "./posts.resolver";
import { SinglePostComponent } from "./single-post/single-post.component";

const routes: Routes = [
  { path: '', component: PostsListComponent, resolve: {posts: PostsResolver} },
  { path: 'add', component: AddPostComponent },
  { path: 'edit/:id', component: EditPostComponent, resolve: {posts: PostsResolver} },
  { path: 'details/:id', component: SinglePostComponent, resolve: {posts: PostsResolver} },
]

const entityMetadata: EntityMetadataMap = {
  Post: {
    sortComparer: sortByName,
    entityDispatcherOptions: {
      optimisticUpdate: true,
      optimisticDelete: false,
    }
  }
}

function sortByName (prev: Post, next: Post) {
  return next.title.localeCompare(prev.title)
}

@NgModule({
  declarations: [
    PostsListComponent,
    SinglePostComponent,
    EditPostComponent,
    AddPostComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  providers: [PostsDataService, PostsResolver],
})
export class PostsModule {
  constructor(
    entityDefinitionService: EntityDefinitionService,
    entityDataService: EntityDataService,
    postsDataService: PostsDataService
  ) {
    entityDefinitionService.registerMetadataMap(entityMetadata)
    entityDataService.registerService('Post', postsDataService)
  }
}