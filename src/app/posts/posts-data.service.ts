import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { DefaultDataService, HttpUrlGenerator } from "@ngrx/data";
import { Update } from "@ngrx/entity";
import { map, Observable } from "rxjs";
import { Post } from "../models/post.model";


@Injectable()
export class PostsDataService extends DefaultDataService<Post> {
  private baseUrl = 'https://test-project-bcf60-default-rtdb.europe-west1.firebasedatabase.app'
  private path = '/posts.json'

  constructor(
    http: HttpClient,
    httpUrlGenerator: HttpUrlGenerator
  ) {
    super('Post', http, httpUrlGenerator)
  }

  override getAll(): Observable<Post[]> {
    return this.http.get<Post[]>(`${this.baseUrl}${this.path}`)
      .pipe(
        map((data) => {
          const posts: Post[] = []

          for (const key in data) {
            posts.push({...data[key], id: key})
          }

          return posts
        })
      )
  }

  override add(post: Post): Observable<Post> {
    return this.http.post<{name: string}>(`${this.baseUrl}${this.path}`, post)
      .pipe(
        map(data => ({...post, id: data.name}))
      )
  }

  override update(post: Update<Post>): Observable<Post> {
    const path = `/posts/${post.id}.json`

    return this.http.put<Post>(`${this.baseUrl}${path}`, {...post.changes})
  }

  override delete(id: string): Observable<string> {
    const path = `/posts/${id}.json`

    return this.http.delete<string>(`${this.baseUrl}${path}`)
      .pipe(
        map(data => id)
      )
  }
}