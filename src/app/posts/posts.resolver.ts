import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { first, map, mergeMap, Observable, of, tap } from "rxjs";
import { PostsService } from "./posts.service";

@Injectable()
export class PostsResolver implements Resolve<boolean> {
  constructor(
    private postsService: PostsService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    // return this.postsService.loaded$.pipe(
    //   mergeMap(loaded => {
    //     if(loaded) {
    //       return of(true)
    //     }

    //     return this.postsService.getAll().pipe(
    //       map(posts => !!posts)
    //     )
    //   }),
    //   first()
    // )
    return this.postsService.loaded$.pipe(
      tap(loaded => {
        if (!loaded) {
          this.postsService.getAll()
        }
      }),
      first()
    )
  }
  
}